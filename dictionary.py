#!/usr/bin/env python3
"this is a wapper around pyglossary"
import pyglossary
import packaging.version

PYGLOSVERSION = packaging.version.parse(pyglossary.VERSION)

if PYGLOSVERSION.major >= 4:
    pyglossary.Glossary.init()


class Dictionary:
    "Handelt Dictionary"

    def __init__(self, infos: dict, filename, dict_format="Stardict"):
        self.format = dict_format
        self.glos = pyglossary.Glossary()

        for k, value in infos.items():
            self.glos.setInfo(k, value)
        ext = (
            self.glos.findPlugin(dict_format).ext
            if PYGLOSVERSION.major >= 4
            else next(
                (k for k, v in self.glos.extFormat.items() if v == dict_format), ""
            )
        )
        self.filename = filename if filename.endswith(ext) else filename + ext
        self.funcs = []
        self._freq=None

    def __len__(self):
        return len(self.glos)

    def add_handler(self, func):
        """
        add function that manatulate definiation, the first funkion  get string,
        the last function  shout get the output string
        """
        self.funcs.append(func)

    def add_entry(self, word, defi, defi_format="m", alti=None):
        "add a dict entry"
        alti = (alti if isinstance(alti, list | dict | set) else [alti]) if alti else []
        print(f"add Entry: {word}")
        if PYGLOSVERSION.major < 4:
            self._count_defi_formats(defi_format)

        for func in self.funcs:
            defi = func(defi)

        entry = self.glos.newEntry(word.strip(), defi, defi_format)

        for alt in alti:
            entry.addAlt(alt.strip())
        self.glos.addEntryObj(entry)

    def _count_defi_formats(self,obj):
        "workaround for pyglossary3, count use the diverent devi_format"
        if self._freq:
            if obj in self._freq:
                self._freq[obj]+=1
            else:
                self._freq[obj]=1
        else:
            self._freq={obj:1}
        return self._freq
    def _most_used(self):
        "workaround for pyglossary give bach most used defi format"
        if self._freq:
            return max(self._freq,key=self._freq.get)
        return ""

    def write(self):
        "Write Dictinary to Disk"
        option={}
        if PYGLOSVERSION.major >= 4:
            self.glos.sortWords("stardict")
        elif self.format== "Stardict":
            option={"sametypesequence":self._most_used()}

        self.glos.write(self.filename, self.format,**option)

    def read(self, filename, dict_format="Stardict", defi_format="m"):
        "Read Dictonare and appliy add_Hander funcion on it, and add to this dictionary"

        ext = (
            self.glos.findPlugin(dict_format).ext
            if PYGLOSVERSION.major >= 4
            else next(
                (k for k, v in self.glos.extFormat.items() if v == dict_format), ""
            )
        )
        tmp = pyglossary.Glossary()
        tmp.read(filename if filename.endswith(ext) else filename + ext, dict_format)

        def _run_func(data, funcs):
            "helper applay function"

            for func in funcs:
                data = func(data)

            return str(data)
        for entry in tmp:
            entry.editFuncDefi(lambda a: _run_func(a, self.funcs))
            if PYGLOSVERSION.major >= 4:
                entry.defiFormat=defi_format
            else:
                entry.setDefiFormat(defi_format)
                self._count_defi_formats(defi_format)
            self.glos.addEntryObj(entry)
