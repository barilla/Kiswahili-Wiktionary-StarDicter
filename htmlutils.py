#!/usr/bin/env python3
"method to grep and mod wikinaryhtmlcode"
import re
import copy
import textwrap
import bs4
import humanfriendly
import humanfriendly.terminal.html

PARSER = "lxml"  # "lxml", "html5lib", "html.parser"
HtmlToAnsi = humanfriendly.terminal.html.HTMLConverter()


def grep_language(htmlstr: str, language: list):
    "Process html entrie"
    html = bs4.BeautifulSoup(htmlstr, features=PARSER)
    captures = html.find_all(lambda tag: tag.has_attr("id") and tag["id"] in language)

    if len(captures) < 1:
        return None

    newhtml = bs4.BeautifulSoup("<html><body>", features=PARSER)

    for cap in captures:
        ele = cap.parent
        # remove lagunge titel if only one laguage

        if isinstance(language, str) or len(language) == 1:
            ele = ele.next_sibling

        while ele is not None:
            newhtml.html.body.append(copy.copy(ele))
            ele = ele.next_sibling

            if ele is None or ele.name == "h2":
                break

    return newhtml


def remove_conjugation_table(html: bs4.BeautifulSoup):
    "remove Conjugation table"
    forms = "Declension", "Inflection", "Conjugation"
    elements = html.find_all(id=[re.compile(f"^{form}.*") for form in forms])

    for ele in elements:
        ele = ele.find_parent(re.compile("^h[1-9]+$"))

        while ele is not None:
            delele = ele
            ele = ele.next_sibling
            delele.replace_with("")

            if (
                ele is not None
                and ele.name is not None
                and bool(re.match("^h[1-9]+$", ele.name))
            ):
                break


def cleanup(html: bs4.BeautifulSoup):
    "clean up the unused element"
    remove_conjugation_table(html)
    remove_big_table(html, 4, 4)

    # remove image, style  Edit audio

    for args in [
        {"name": ["img", "style"]},
        {
            "class_": [
                "audiotable",
                "mw-editsection",
                "NavFrame",
                "noprint",
                "sister-wikipedia",
                "sister-project",
            ]
        },
        {lambda ele: ele.name == "table" and "Swahili numbers" in ele.strings},
        {
            "name": "a",
            "string": [re.compile(r"\[edit\]|EDIT", re.I), re.compile(r"\[[0-9]+\]")],
        },
    ]:
        for ele in (
            html.find_all(**args) if isinstance(args, dict) else html.find_all(*args)
        ):
            ele.replace_with("")

    # remove empthy list

    for ele in html.find_all(["ol", "ul", "li", "dd"]):
        if len(ele.text.strip()) < 1:
            ele.replace_with("")

    # remove  comment

    for ele in html.findAll(text=lambda text: isinstance(text, bs4.element.Comment)):
        ele.replace_with("")

    # skip tags
    invalid_tags = ["div", "span", "a", "link"]

    # remove nested
    tags = ["u", "s", "b", "strong"]

    for tag in html.findAll(True):
        # remove nested

        if tag and tag.name in tags:
            for cont in tag.contents:
                if cont.name == tag.name:
                    cont.unwrap()
        # skip tags

        if tag.name in invalid_tags:
            tag.unwrap()
        # remove empthy tag

        if tag.name not in ["br"] and len(tag.get_text(strip=True)) == 0:
            tag.extract()

    remove_empthy_segment(html)

    return html


def remove_big_table(html, lenght=4, width=4):
    "remove tabel that has more line than lenght or colums than width"

    for table in html.find_all("table"):
        nr_line = len(table.find_all("tr"))

        if nr_line >= lenght:
            table.replace_with("")

            continue

        if nr_line > 0 and len(table.tr.find_all(["td", "th"])) >= width:
            table.replace_with("")


def remove_empthy_segment(html):
    "remove <h?> element follow each without content between"
    h_ele = [f"h{x}" for x in range(5)]

    for tag in html.find_all(h_ele)[::-1]:
        sib = next(
            (
                x
                for x in tag.next_siblings
                if (not (isinstance(x, bs4.NavigableString)) and x)
                or (isinstance(x, bs4.NavigableString) and x.strip())
            ),
            None,
        )

        if sib is None or (sib.name in h_ele and int(tag.name[1]) >= int(sib.name[1])):
            tag.replace_with("")

    return html


def to_ansi(html: str | bs4.BeautifulSoup) -> str:
    "Create Formated ANSI TEXT"
    html = (
        bs4.BeautifulSoup(html, features=PARSER)
        if isinstance(html, str)
        else copy.copy(html)
    )

    for tag in html.find_all([f"h{x}" for x in range(5)]):

        tag.insert_before("\n\n")
        tag.insert_after("\n\n")
        tag.wrap(html.new_tag("u"))

        if tag.name.endswith("3"):
            tag.wrap(html.new_tag("b"))

        if tag.name.endswith("4"):
            tag.wrap(html.new_tag("b"))
            tag.wrap(html.new_tag("i"))

        if tag.name.endswith("6"):
            tag.wrap(html.new_tag("i"))
        tag.unwrap()

    for tag in html.find_all(["ol", "ul", "dd"]):
        tag.insert_after("\n")
        tag.insert_before("\n\n")

    listobject(html)

    return HtmlToAnsi(humanfriendly.text.dedent(str(html)))


def listobject(html):
    "indent and format listobject"
    listtyp = {"ol": "li", "ul": "li", "dl": "dd"}
    prefix = {"ol": "{:>3}) ", "ul": "   * ", "dl": " " * 5}

    for listobj in html.find_all(listtyp.keys())[::-1]:
        indent = " " * 5 * len(listobj.find_parents(listtyp.keys()))

        for i, lentry in enumerate(
            listobj.findChildren(listtyp[listobj.name]), start=1
        ):
            lentry.insert_before(indent + prefix[listobj.name].format(i))
            add_indent(lentry, indent)

            if not (
                lentry.nextSibling
                and lentry.nextSibling.text
                and "\n" in lentry.nextSibling.text
            ):
                lentry.insert_after("\n")
            lentry.unwrap()
        listobj.unwrap()


def add_indent(html, indent):
    "add indent for each newline element"

    for ele in html.find_all(HtmlToAnsi.BLOCK_TAGS):
        ele.insert_after(indent)

    for ele in html.find_all(string=re.compile(r"\n")):
        ele = f"\n{indent}".join(ele.split("\n"))


def wrap(txt: str, width: int = 120, **kwargs) -> str:
    "warp eachline at width, and accept the indent"
    txt = txt if isinstance(txt, str) else str(txt)
    pattern = re.compile(r"^\s*([0-9]\)|[\+\-\*])?\s")
    out = []

    for line in txt.splitlines():
        indent = re.search(pattern, line)
        out += textwrap.wrap(
            line,
            width,
            subsequent_indent=" " * len(indent[0]) if indent else "",
            **kwargs,
        )

    return "\n".join(out)
