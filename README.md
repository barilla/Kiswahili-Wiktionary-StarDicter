Kiswahili Wiktionary Dicter
===========================
This Dicter create a StarDict Dictionary from Wikipedia, primary aimed for Kiswahili with ANSI and HTML Version

Main goals
----------
* ANSI Output
* Clean HTML Output
* For use with [Koreader](https://github.com/koreader/koreader) and [sdcv](https://github.com/Dushistov/sdcv)
* All awesome Declension, Inflection, Conjugation tables linked as synonyms
* [kaikki.org](https://kaikki.org/dictionary/Swahili/index.html) Based
* [pyglossary](https://github.com/ilius/pyglossary) Based

Inspiration
------------

Some time ago I was hacking around  on [wiktionary-to-stardict](https://gitlab.com/artefact2/wiktionary-to-stardict), replacing some elements with python, at that time my motivation was to create a clean plain text (markdown) and clean filtered Html output, the resulting dictionary was good enough for me, but not more.

When I noticed [#9370](https://github.com/koreader/koreader/pull/9370#issuecomment-1200411581)/[czech-dictionary-extender](https://github.com/Vuizur/czech-dictionary-extender), it was clear for me to create a rewrite, with [pyglossary](https://github.com/ilius/pyglossary) and [kaikki.org](https://kaikki.org/dictionary/Swahili/index.html) dump.

Next Steps
----------
- [x] Porting to the latest version of pyglossary, My development machine is Debian, but pyglossary version on Debian(testing) is still on [3.2](https://packages.debian.org/search?keywords=pyglossary), the current release is [4.5](https://github.com/ilius/pyglossary/releases/tag/4.5.0).  
- [x] Test with sdcv 
- [x] Test for KoReader
- [ ] Release a Kiswahili Dict
  - [ ] Quality check
  - [ ] Random Output Check for all grammatical categories.

Future Idea
-----------
* In HTML Version, allow image tag and store them in `res/` folder.
* Why not the same for Pronunciation audio, but not a Use-Case for me, feel free to contribute.
* Export other Dictionary Format, witch ones?
* Synonyms Table get maybe to big. Conjugation could be reduced to common use Forms.

