#! /usr/bin/env python3
"""
A tool to creat a Kiswahili  StarDict from Wikinary with pyglossary
This is the main rutine
"""
import os
import timeit
import zipfile
import wikinary_fetcher
import htmlutils
import dictionary


def create_and_fetch_html_dict(
    languages, infos, filename, source, dict_format="Stardict"
):
    "feetch wikinary and create a dictinary"
    languages = languages if isinstance(languages, list | set) else [languages]
    glos = dictionary.Dictionary(infos, filename, dict_format)
    glos.add_handler(lambda data: htmlutils.grep_language(data, languages))
    glos.add_handler(htmlutils.cleanup)
    # from bs4 object to htmlstring
    glos.add_handler(lambda data: data if isinstance(data, str) else str(data))
    start = timeit.default_timer()
    wikinary_fetcher.fetch(glos, source, languages, "h")
    print(f"runime {timeit.default_timer()-start} seconds")
    print(f"{(timeit.default_timer()-start)/len(glos)} seconds by entry")
    glos.write()


def create_ansi_dict_from_html_dict(
    infos, source_file, out_file, dict_format="Stardict"
):
    "create ansi Dictionary form a html version"
    glos = dictionary.Dictionary(infos, out_file, dict_format)
    glos.add_handler(htmlutils.to_ansi)
    glos.add_handler(htmlutils.wrap)
    glos.read(source_file, dict_format, "m")
    glos.write()


def mk_path(*kwargs):
    "crate dirs"
    filepath = os.path.join(*kwargs)

    if not os.path.exists(filepath):
        os.makedirs(filepath, exist_ok=True)


def zipfolder(filepath, zipname):
    "zip a folder"
    with zipfile.ZipFile(zipname, "w") as zipobj:
        for file in os.listdir(filepath):
            zipobj.write(os.path.join(filepath, file), arcname=file)


if __name__ == "__main__":
    SOURCE = "/tmp/mozilla/download/kaikki.org-dictionary-Swahili.json"
    ONLINESOURCE = (
        "https://kaikki.org/dictionary/Swahili/kaikki.org-dictionary-Swahili.json"
    )
    LANGUAGE = "Swahili"
    METADATA = {
        "title": "Kiswahili Wiktionary Dictionary (sw-en)",
        "author": "barilla",
        "description": "extended Kiswahili dictionary with all the awesome declension, "
        "inflection, conjugation tables linked as synonyms",
    }
    PATH = "/tmp/dict"
    FILENAME = "SWWikinaryDict"
    DICTFORMAT = "Stardict"

    print("create html_dict")
    mk_path(PATH, "html")
    path = os.path.join(PATH, "html", FILENAME)
    create_and_fetch_html_dict(
        LANGUAGE,
        METADATA,
        path,
        SOURCE if os.path.isfile(SOURCE) else ONLINESOURCE,
        DICTFORMAT,
    )
    mk_path(PATH, "release")
    zipfolder(
        os.path.join(PATH, "html"), os.path.join(PATH, "release", FILENAME + ".zip")
    )

    print("create ansi_dict")
    mk_path(PATH, "ansi")
    create_ansi_dict_from_html_dict(
        METADATA | {"title": METADATA["title"] + " ANSI"},
        path,
        os.path.join(PATH, "ansi", FILENAME),
        DICTFORMAT,
    )
    zipfolder(
        os.path.join(PATH, "ansi"),
        os.path.join(PATH, "release", FILENAME + "_ANSI.zip"),
    )
