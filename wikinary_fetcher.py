#!/usr/bin/env python3
"""
A tool to creat a Kiswahili  StarDict from Wikinary with pyglossary"
Fetch Wikinary async
"""
import asyncio
import json
import httpx

HEADER = {
    "user-agent": "My-Sw-Dict/0.1 (https://github.com/barilla-aldente;"
    + "barilla@pfaditriesen.li) python3-requests/2.27",
    "Accept-Encoding": "br;q=1.0, zstd;q=0.9, gzip;q=0.5, deflate;q=0.3",
}

LOCK = asyncio.Lock()


async def producer(queue, sourefile):
    "fill the queue with Lines frim kaikki"
    remote=sourefile.strip().startswith(("http://","https://"))
    with httpx.stream("GET", sourefile) if remote else  open(sourefile, encoding="utf-8") as file:
        tmp = []
        for line in file.iter_lines() if remote else file:
            if not (entry := json.loads(line)):
                continue

            if len(tmp) == 0 or tmp[0]["word"] == entry["word"]:
                tmp.append(entry)

            if tmp[0]["word"] != entry["word"]:
                await queue.put(tmp)
                tmp = [entry]

        if tmp:
            await queue.put(tmp)


async def consumer(
    queue, dictionary, languages: str | list | dict | set, defi_format="h"
):
    "read queue and make wikinary request"
    languages = "|" if isinstance(languages, list | set | dict) else languages
    async with httpx.AsyncClient(timeout=30) as client:
        while True:
            token = await queue.get()
            word = token[0]["word"]
            i = 0

            url = (
                "https://en.wiktionary.org/w/api.php?action=parse&format=json&maxlag=8&"
                + f"page={word}&prop=text&sectiontitle={languages}&formatversion=2"
            )

            while True:
                i += 1 if i < 12 else 0
                try:
                    res = await client.get(url, headers=HEADER)

                    if "error" in res.json():
                        print(res.json()["error"])

                        if res.json()["error"]["code"] in ["ratelimited", "maxlag"]:
                            print(f"sleep {5*i} second")
                            await asyncio.sleep(5 * i)
                    else:
                        break
                except httpx.HTTPError as error:
                    print(f"Http Error by fetching {word}, retry in {5*i} sec\n",error)
                    await asyncio.sleep(5*1)

            if res.status_code == 200:
                alti = set().union(
                    *(
                        {x["form"] for x in y["forms"] if x["form"].strip()}
                        for y in token
                        if "forms" in y
                    )
                )
                async with LOCK:
                    dictionary.add_entry(
                        word, res.json()["parse"]["text"], defi_format, alti
                    )
            queue.task_done()


def fetch(dictionary, sourefile, languages="Swahili", defi_format="h"):
    "fetch wikinary"
    asyncio.run(run(dictionary, sourefile, languages, defi_format))


async def run(dictionary, sourefile, languages="Swahili", defi_format="h"):
    "queue strarter"
    queue = asyncio.Queue(10)
    producers = [asyncio.create_task(producer(queue, sourefile))]
    consumers = [
        asyncio.create_task(consumer(queue, dictionary, languages, defi_format))
        for _ in range(3)
    ]

    await asyncio.gather(*producers)
    print("---- done producing")
    await queue.join()

    for con in consumers:
        con.cancel()
